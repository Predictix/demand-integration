block(`coefficients) {

  alias_all(`lb:web:config:service),
  alias_all(`lb:web:config:service_abbr),
  alias_all(`lb:web:config:delim),
  alias_all(`lb:web:delim:schema),
  alias_all(`lb:web:delim:schema_abbr),
  alias_all(`lb:web:delim:binding),
  alias_all(`lb:web:delim:filter),
  alias_all(`lb:web:delim:binding_abbr),
  alias_all(`tdx_helpers:predicate_specs),


  sealed(`{

    fb(fb) -> file_binding(fb).
  }),


  clauses(`{ 

    bias_file_definition_name[] = s -> string(s).
    bias_file_definition_name[] = "bias".

    binds_service_by_prefix(meta:workspace:me[],bias_file_definition_name[]) <- .

    file_definition_by_name[bias_file_definition_name[]] = fd,
    file_definition(fd) {
      file_delimiter[] = "|",
      column_headers[] = "cluster,bias", 
      
      file_column_format["cluster"] = "int",
      file_column_format["bias"] = "float"
    }, 

    fb(fb),
    file_binding_by_name[bias_file_definition_name[]] = fb,
    file_binding(fb) {
      file_binding_definition_name[] = bias_file_definition_name[],
      predicate_binding_by_name["forecasting:coefficients:bias"] = predicate_binding(_) {
        predicate_binding_columns[] = "cluster,bias"
      }
    }.   
  }),


  clauses(`{ 

    centroids_file_definition_name[] = s -> string(s).
    centroids_file_definition_name[] = "centroids".

    binds_service_by_prefix(meta:workspace:me[],centroids_file_definition_name[]) <- .

    file_definition_by_name[centroids_file_definition_name[]] = fd,
    file_definition(fd) {
      file_delimiter[] = "|",
      column_headers[] = "centroid,index,value", 
      
      file_column_format["centroid"] = "int",
      file_column_format["index"] = "int",      
      file_column_format["value"] = "float"
    }, 

    fb(fb),
    file_binding_by_name[centroids_file_definition_name[]] = fb,
    file_binding(fb) {
      file_binding_definition_name[] = centroids_file_definition_name[],
      predicate_binding_by_name["forecasting:coefficients:centroid"] = predicate_binding(_) {
        predicate_binding_columns[] = "centroid,index,value"        
      }
    }.   
  }),


  clauses(`{ 

    alpha_file_definition_name[] = s -> string(s).
    alpha_file_definition_name[] = "alpha".

    binds_service_by_prefix(meta:workspace:me[],alpha_file_definition_name[]) <- .

    file_definition_by_name[alpha_file_definition_name[]] = fd,
    file_definition(fd) {
      file_delimiter[] = "|",
      column_headers[] = "cluster,feature,value", 
      
      file_column_format["cluster"] = "int",
      file_column_format["feature"] = "string",      
      file_column_format["value"] = "float"
    }, 

    fb(fb),
    file_binding_by_name[alpha_file_definition_name[]] = fb,
    file_binding(fb) {
      file_binding_definition_name[] = alpha_file_definition_name[],
      predicate_binding_by_name["forecasting:coefficients:alpha"] = predicate_binding(_) {
        predicate_binding_columns[] = "cluster,feature,value",
        column_binding_by_arg[1] =
          column_binding(_) {
            column_binding_import_function[] = "feature_sampling:features:key2Feature"
          }
      }
    }.   
  }),


  clauses(`{ 

    beta_file_definition_name[] = s -> string(s).
    beta_file_definition_name[] = "beta".

    binds_service_by_prefix(meta:workspace:me[],beta_file_definition_name[]) <- .

    file_definition_by_name[beta_file_definition_name[]] = fd,
    file_definition(fd) {
      file_delimiter[] = "|",
      column_headers[] = "cluster,feature,index,value", 
      
      file_column_format["cluster"] = "int",
      file_column_format["feature"] = "string",
      file_column_format["index"] = "int",
      file_column_format["value"] = "float"
    }, 

    fb(fb),
    file_binding_by_name[beta_file_definition_name[]] = fb,
    file_binding(fb) {
      file_binding_definition_name[] = beta_file_definition_name[],
      predicate_binding_by_name["forecasting:coefficients:beta"] = predicate_binding(_) {
        predicate_binding_columns[] = "cluster,feature,index,value",
        column_binding_by_arg[1] =
          column_binding(_) {
            column_binding_import_function[] = "feature_sampling:features:key2Feature"
          }        
      }
    }.   
  })

} <-- .
