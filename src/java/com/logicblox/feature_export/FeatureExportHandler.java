package com.logicblox.bloxweb;

import com.logicblox.bloxweb.features_extraction.FeaturesExtraction;
import java.io.File;
import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Functions;
import com.google.common.util.concurrent.AsyncFunction;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.logicblox.bloxweb.util.IOUtils;
import com.logicblox.s3lib.CloudStoreClient;

public class FeatureExportHandler extends DefaultProtoBufHandler {
  
   @Override
   protected Message.Builder getRequestBuilder()
   {
      return FeaturesExtraction.Extract.newBuilder();
   }

   @Override
   protected Message.Builder getResponseBuilder()
   {
      return FeaturesExtraction.Response.newBuilder();
   }

  @Override
  public ListenableFuture<ProtoBufExchange> handle(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final ProtoBufExchange protoExchange) throws ServletException, IOException,
      InvalidProtocolBufferException, InvalidRequestException {
    
    // Execute the regular handling code
    final ListenableFuture<ProtoBufExchange> future =
        super.handle(request, response, protoExchange);

    final FeaturesExtraction.Extract rq = (FeaturesExtraction.Extract) protoExchange.getRequestMessage();

    // get a flag to determine if a file needs to be uploaded to s3
    boolean upload = false;
    if (rq.hasUpload())
       upload = rq.getUpload();

    if (!upload) 
       return future;
    else
      // Post-process the response asynchronously
      return Futures.transform(future, new AsyncFunction<ProtoBufExchange, ProtoBufExchange>() {
      
      @Override
        public ListenableFuture<ProtoBufExchange> apply(ProtoBufExchange exchange) throws Exception {
        
          final FeaturesExtraction.Extract rq = (FeaturesExtraction.Extract) exchange.getRequestMessage();
          // obtain local temp file
          final File file = new File(rq.getExtract());

          // uri for s3 destination
          if (!rq.hasS3Path())
            throw new InvalidProtocolBufferException("The request message doesn't have field s3path");

          final URI uri = new URI(rq.getS3Path()); 
        
          // Create a client to upload the file to the URI
          final CloudStoreClient client =
              getServiceConfig().getContext().getCSClientFromScheme(uri.getScheme());
        
          return Futures.transform(IOUtils.upload(client, uri, null, file, false),
              Functions.constant(exchange));
        }
      });
  } 

}
