SETTING|VALUE
FORECAST_SETTINGS_FILE|data/common/forecast-domain-settings.dat
EXCLUDED_ATTRIBUTES_FILE|excluded-attributes-default
EXCLUDED_FEATURE_FILE|excluded-features-default
MODEL|FaMa
MODEL_PARAMETERS_FILE|ml-parameters-default.dlm
SAMPLING_FRACTION|1.0
START_LEARNING_TYPE|cold
