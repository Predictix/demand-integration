#!/usr/bin/env bash

#--- Define workspace name and import it
ws_name=${1}

lb web-server load -c out/lb-web-server.config

mkdir -p out/workspaces/${ws_name}
tar xvfz out/workspaces/demand-template.tgz -C out/workspaces/${ws_name}
lb import-workspace ${ws_name} out/workspaces/${ws_name} --overwrite

lb exec ${ws_name} '^meta:workspace:me[]="/'${ws_name}'".'
lb web-server load-services -w ${ws_name}


#--- Set data folders path & import data
data_dir=$PWD/data
common_dir=$PWD/data/common
infor_dir=$PWD/data/infor

log_dir=$PWD/data/log/
mkdir -p $log_dir

echo "--- Importing the standard temporal attributes file"
time lb web-client import http://localhost:8080/${ws_name}/temporal_attributes \
  -i file://${infor_dir}/infor_temporal_attributes.dat -p -n -T 28800 > ${log_dir}/temporal_attributes.log 2>&1

echo "--- Importing calendar period file"
time lb web-client import http://localhost:8080/${ws_name}/period \
  -i file://${data_dir}/period.dat -p -n -T 28800 > ${log_dir}/period.log 2>&1

echo "--- Importing product file"
time lb web-client import http://localhost:8080/${ws_name}/product \
  -i file://${data_dir}/product.dat -p -n -T 28800 > ${log_dir}/product.log 2>&1

echo "--- Importing location file"
time lb web-client import http://localhost:8080/${ws_name}/location \
  -i file://${data_dir}/location.dat -p -n -T 28800 > ${log_dir}/location.log 2>&1
  
echo "--- Importing customer file"
time lb web-client import http://localhost:8080/${ws_name}/customer \
  -i file://${data_dir}/customer.dat -p -n -T 28800 > ${log_dir}/customers.log 2>&1
  
echo "--- Importing sales file"
time lb web-client import http://localhost:8080/${ws_name}/sales \
  -i file://${data_dir}/sales_full.csv -p -n -T 28800 > ${log_dir}/sales.log 2>&1

echo "--- Importing demand+ metrics (forecasts, etc)"
time lb web-client import http://localhost:8080/${ws_name}/dp_metrics \
  -i file://${data_dir}/demand-plus-metrics.csv -p -n -T 28800 > ${log_dir}/dp.log 2>&1


echo "--- Importing  period data"
time lb web-client import http://localhost:8080/${ws_name}/period_2_week \
  -i file://${data_dir}/period_to_week.csv -p -n -T 28800 > ${log_dir}/period.log 2>&1
