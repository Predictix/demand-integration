#! /usr/bin/env bash

set -e

partition=${1}
ws_name=${2}
settings_file=${3}

#--- Read all relevant parameter settings 
get-param() {
  value=$(grep -w ${1} ${settings_file} | cut -d\| -f2 )
  echo ${value}
}

forecast_settings_file=$(get-param "FORECAST_SETTINGS_FILE")
excluded_attributes_file=$(get-param "EXCLUDED_ATTRIBUTES_FILE")
excluded_features_file=$(get-param "EXCLUDED_FEATURE_FILE")
model=$(get-param "MODEL")
model_parameters_file=$(get-param "MODEL_PARAMETERS_FILE")
sampling_fraction=$(get-param "SAMPLING_FRACTION")
start_learning=$(get-param "START_LEARNING_TYPE")


#--- Set up run folder structure
export EXECUTION_HOME=${PWD}
export FEATURE_EXT_HOME=${EXECUTION_HOME}/export/${partition}
export LWFM_PATH=${EXECUTION_HOME}/upstream/foula/bin

rm -rf ${FEATURE_EXT_HOME} && mkdir -p ${FEATURE_EXT_HOME}


function set_forecast_domain(){
  echo "--- Importing forecasting settings"
  time lb web-client import http://localhost:8080/${ws_name}/forecast_domain_settings \
    -i file://${EXECUTION_HOME}/${forecast_settings_file} -p -n -T 28800   
}

function export_train_data(){

  #--- Note: we export traing set for -ALL- skus in the workspace!
  echo "--- Export the training set"
  echo '{"extract":"'${FEATURE_EXT_HOME}'/train.pb_0","is_train":true,"prod_rollup_label":"ALL","s3path":"/mnt/Data/train","upload":false, "header":"header,meta:3,sparse:double:%feat_count,","zero_sampling_fraction":'${sampling_fraction}'}'| lb web-client call http://localhost:8080/${ws_name}/sample_features -T 28800
}

function export_test_data(){

  #--- Note: we export traing set for -ALL- skus in the workspace!
  echo "--- Export the test set"
  echo '{"extract":"'${FEATURE_EXT_HOME}'/test.pb_0","is_train":false,"prod_rollup_label":"ALL","s3path":"/mnt/Data/train","upload":false, "header":"header,meta:3,sparse:double:%feat_count"}'| lb web-client call http://localhost:8080/${ws_name}/sample_features -T 28800
}

function export_feature_dictionary(){

  echo "--- Export the features dictionary"
  lb web-client export http://localhost:8080/${ws_name}/features_dict \
    -T 28800 -n -o ${FEATURE_EXT_HOME}/features_dict.out
  
  sed -i "s/,//g"  ${FEATURE_EXT_HOME}/features_dict.out
  sed -i "s/ /-/g" ${FEATURE_EXT_HOME}/features_dict.out
  sort -t\| -nk2 ${FEATURE_EXT_HOME}/features_dict.out | sed "1,2d" | sed "s/|/,/g" > ${FEATURE_EXT_HOME}/attribute_index
}

function run_fama(){

  echo "--- ML with setting: $model - ${sampling_fraction} - ${start_learning}"
  
  echo "--- ML with excluded attributes file: ${excluded_attributes_file}"
  cp ${EXECUTION_HOME}/config/${excluded_attributes_file} ${FEATURE_EXT_HOME}/excluded_attributes
  echo "--- ML with excluded features file: ${excluded_features_file}"  
  cp ${EXECUTION_HOME}/config/${excluded_features_file} ${FEATURE_EXT_HOME}/excluded_features
  echo "--- ML with model settings file: ${model_parameters_file}"  
  cp ${EXECUTION_HOME}/config/${model_parameters_file} ${FEATURE_EXT_HOME}/model_parameters.dlm

  if [ "${start_learning}" == "warm" ] ; then
    echo "--- Warm/Incremental learning: updating coefficients"
	  mkdir -p ${FEATURE_EXT_HOME}/warmup
    python script/revert-and-update-coeffs.py \
      --dictionary_file ${FEATURE_EXT_HOME}/features_dict.out \
      --input_folder ${EXECUTION_HOME}/warmup --output_folder ${FEATURE_EXT_HOME}/warmup
  fi

  bash ${EXECUTION_HOME}/script/${model}.sh ${FEATURE_EXT_HOME} ${sampling_fraction} ${start_learning} > ${FEATURE_EXT_HOME}/nohup.log
}

function import_ml_coeffs(){
  
  echo "--- Import ml coefficients (generate idb fitted/forecast)"
      
  if [ ! -f "${FEATURE_EXT_HOME}/coeffscentroid.dlm" ] ; then 
    echo "centroid|index|value" > ${FEATURE_EXT_HOME}/coeffscentroid.dlm 
  fi
 
  lb workflow run --workspace /coeffs-ml-wf  --file src/workflow/ml/import-coeffs-ml.wf --main main --param "ws_name=${ws_name}" --param "coeff_dir=${FEATURE_EXT_HOME}" --auto-terminate 1 --overwrite 
}

function export_forecast(){
  echo "--- exporting forecast"
  lb web-client export http://localhost:8080/${ws_name}/forecast_export?tdx_file_mode="noquotes" \
    -n -T 14400 -o file://${FEATURE_EXT_HOME}/total_forecast.csv
}

function export_accuracy_metrics(){

  echo "--- Export accuracy metrics"
  lb web-client export http://localhost:8080/${ws_name}/prod_accuracy_metrics \
    -n -T 14400 -o ${FEATURE_EXT_HOME}/prod_accuracy_metrics.csv
  lb web-client export http://localhost:8080/${ws_name}/overall_accuracy_metrics \
    -n -T 14400 -o ${FEATURE_EXT_HOME}/overall_accuracy_metrics.csv
}

function export_tableau_reports(){
    
  echo "--- Export the tableau report"
  lb web-client export http://localhost:8080/${ws_name}/tableau \
    -n -T 14400 -o file://${FEATURE_EXT_HOME}/tableau_report.csv
}


main(){
  
  #--- Set up & run ML
  time set_forecast_domain
  time export_feature_dictionary
  time export_train_data
  time export_test_data
  time run_fama 
  time import_ml_coeffs  

  #--- Exports
  time export_forecast
  time export_accuracy_metrics
  time export_tableau_reports
}

main
