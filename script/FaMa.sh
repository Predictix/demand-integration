#!/bin/bash

path=$1
sampling_fraction=$2
start=$3

TRAIN=${path}/train.pb_0
TEST=${path}/test.pb_0
DICT=${path}/attribute_index

LWFM=$PWD/upstream/foula/bin

export OMP_NUM_THREADS=`expr $(nproc) - 1`


generateExclude() {
  rm -f ${path}/excluded_indices
  for d in $(cat ${path}/excluded_attributes)
  do
    echo Excluding $d
    grep "^$d:" ${DICT} | cut -d \, -f 2 >> ${path}/excluded_indices
  done  
  
  for d in $(cat ${path}/excluded_features)
  do
    echo Excluding $d
    grep "^$d" ${DICT} | cut -d \, -f 2 >> ${path}/excluded_indices
  done  
}

get-param() {
  value=$(grep -w ${1} ${path}/model_parameters.dlm | cut -d\| -f2 )
  echo ${value}
}

#--- Set FaMa parameters for the run
RANK=$(get-param "RANK")
SGD_ITER=$(get-param "SGD_ITER")
SGD_ITER_WARM=$(get-param "SGD_ITER_WARM")
CG_ITER=$(get-param "CG_ITER")
CG_ITER_WARM=$(get-param "CG_ITER_WARM")
ETA=$(get-param "ETA")
ETA_WARM=$(get-param "ETA_WARM")
REG_TYPE=$(get-param "REG_TYPE")
REG_PARAM=$(get-param "REG_PARAM")

runFaMa() {
  ${LWFM}/lwfm.x \
    ${TRAIN} \
    --dictionary=${DICT} \
    --exclude_file=${path}/excluded_indices \
    --max_chunk_size=3gb \
    --order=2 \
    --rank=${RANK} \
    --sgd_iter=${SGD_ITER} \
    --cg_iter=${CG_ITER} \
    --eta=${ETA} \
    --reg_type=${REG_TYPE} \
    --reg_param=${REG_PARAM} \
    --seed=-1 \
    --feature_error=train_error_rate.dlm \
    --output_path="${path}/" \
    --with_weights=${1} \
    --min_forecast=0.0

  mv ${path}/predictions.pb ${path}/fitted.pb
}

rerunFaMa() {
  ${LWFM}/lwfm.x \
    ${TRAIN} \
    --dictionary=${DICT} \
    --exclude_file=${path}/excluded_indices \
    --max_chunk_size=3gb \
    --order=2 \
    --rank=${RANK} \
    --sgd_iter=${SGD_ITER} \
    --cg_iter=${CG_ITER} \
    --eta=${ETA} \
    --reg_type=${REG_TYPE} \
    --reg_param=${REG_PARAM} \
    --seed=-1 \
    --feature_error=train_error_rate.dlm \
    --output_path="${path}/" \
    --min_forecast=0.0 \
    --with_guess

  mv ${path}/predictions.pb ${path}/fitted.pb
}

runFaMaWarm() {
  ${LWFM}/lwfm.x \
    ${TRAIN} \
    --dictionary=${DICT} \
    --exclude_file=${path}/excluded_indices \
    --max_chunk_size=3gb \
    --order=2 \
    --rank=${RANK} \
    --sgd_iter=${SGD_ITER_WARM} \
    --cg_iter=${CG_ITER_WARM} \
    --eta=${ETA_WARM} \
    --reg_type=${REG_TYPE} \
    --reg_param=${REG_PARAM} \
    --seed=-1 \
    --feature_error=train_error_rate.dlm \
    --output_path="${path}/" \
    --with_weights=${1} \
    --min_forecast=0.0 \
    --with_guess \
    --input_path="${path}/warmup/"

  mv ${path}/predictions.pb ${path}/fitted.pb
}

evalFaMa() {
  ${LWFM}/lwfm.x \
    ${TEST} \
    --dictionary=${DICT} \
    --max_chunk_size=3gb \
    --order=2 \
    --rank=${RANK} \
    --feature_error=test_error_rate.dlm \
    --input_path="${path}/" \
    --output_path="${path}/" \
    --eval \
    --min_forecast=0.0

  mv ${path}/predictions.pb ${path}/total.pb
}


main() {
  #--- Excluded attributes and/or features
  generateExclude

  #--- Setting sampling fraction & fit
  test=$(echo "${sampling_fraction} 1.0" | awk '{if ($1 < $2) print "0"; else print "1"}')
  if [ "${start}" == "warm" ] ; then
    if [ "${test}" -eq "1" ] ; then
      runFaMaWarm -1
    else
      runFaMaWarm 0
    fi
  else
    if [ "${test}" -eq "1" ] ; then
      runFaMa -1
    else
      runFaMa 0
    fi
  fi

  #--- Generate forecasts
  evalFaMa
}

main
