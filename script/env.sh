# get script location
if [[ -n $BASH_VERSION ]]; then
  _SOURCED_SCRIPT="${BASH_SOURCE[0]}"
elif [[ -n $ZSH_VERSION ]]; then
  _SOURCED_SCRIPT="${(%):-%N}"
else
  echo "Shell not supported"
  return
fi

export APP_HOME=$(cd $(dirname $_SOURCED_SCRIPT)/.. ; pwd)
export APP_UPSTREAM=${MYUPSTREAM:=$APP_HOME/upstream}

# setup LB environment
export LB_COMPONENTS=${APP_UPSTREAM}/logicblox
source $LB_COMPONENTS/etc/profile.d/logicblox.sh
export LB_CONNECTBLOX_ENABLE_ADMIN=1

