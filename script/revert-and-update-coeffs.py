import argparse
import csv

parser = argparse.ArgumentParser(description="Update FaMa coefficients for incremental runs")
parser.add_argument("--dictionary_file", help="File that maps feature names to indices", type=str, nargs='?', default="./features_dict.out")
parser.add_argument("--input_folder", help="Folder with previous coefficients (in .dlm format)", type=str, nargs='?', default="./")
parser.add_argument("--output_folder", help="Folder with updated  coefficients", type=str, nargs='?', default="./")

args = parser.parse_args()
dictionary_file = args.dictionary_file
input_folder    = args.input_folder 
output_folder   = args.output_folder


def read_dictionary(dictionary_file):
  dictionary = dict()
  feature_count = 0
  
  with open(dictionary_file,"r+") as csv_file_in:
    file_reader = csv.reader(csv_file_in, delimiter='|')
    
    for row in file_reader:
      index = row[1]
      if index == "FEATURE":
        continue
      else:
        desc = row[0]
        dictionary[desc] = int(index)
        feature_count += 1
    
  csv_file_in.close()        
  return (dictionary,feature_count)


def revert_coeffs0(filein, fileout):
  try: 
    fin = open(filein, 'r')
    fin.readline()
  except IOError:
    print "--- INFO: coeff0.dlm file is not available"
    return
      
  fout = open(fileout, 'w')
  fout.write("header,meta:3,double:1,\n")
    
  cluster_bias = {}
  for line in fin:
    cluster = line.split("|")[0].replace('\n','').replace('"','')
    value = line.split("|")[1].replace('\n','').replace('"','')
    cluster_bias[cluster] = value
    fout.write("%s,0,0,%s\n"%(cluster,value))
  
  fin.close()
  fout.close()


def revert_coeffs1(filein, fileout, dictionary,nb_features):
  try: 
    fin = open(filein, 'r')
    fin.readline()
  except IOError:
    print "--- INFO: coeff1.dlm file is not available"
    return
  
  fout = open(fileout, 'w')

  feature_dicos = {}
  cluster_ids = set()
  feature_ids = set()

  for line in fin:
    record = line.replace("\n","").split("|")
    feature_desc = record[1]
    
    if feature_desc in dictionary:
      feature_id = dictionary[feature_desc]
    else:
      continue
    
    cluster_id = int(record[0])
    val = float(record[2])
    feature_dicos[cluster_id,feature_id] = val
    cluster_ids.add(cluster_id)
    feature_ids.add(feature_id)

  header = 'header,meta:3,double:%s,'%(nb_features)
  fout.write('%s\n'%header)

  for cluster in cluster_ids:
    ltp = '%s,0,0'%cluster
    key_to_print=''
    
    for feature in range(0,nb_features):
      if (cluster,feature) in feature_dicos:
        key_value = '%s:%s'%(feature,feature_dicos[(cluster,feature)])
      else:
        key_value = '%s:0.0'%feature
      
      key_to_print = ",".join([key_to_print,key_value])
      ltp = ",".join([ltp,key_value])
    
    fout.write('%s,\n'%ltp)


def revert_coeffs2(filein, fileout, dictionary, nb_features):
  try: 
    fin = open(filein, 'r')
    fin.readline()
  except IOError:
    print "--- INFO: coeff2.dlm file is not available"
    return
  
  fout = open(fileout, 'w')
    
  feature_dicos = {}
  feature_ids = set()
  cluster_ids = set()
  index_ids = set()

  for line in fin :
    record = line.replace("\n","").replace('"','').split("|")
    feature_desc = record[1]
    
    if feature_desc in dictionary:
      feature_id = dictionary[feature_desc]
    else:
      continue
    
    cluster = int(record[0])
    index = int(record[2])
    val = float(record[3])
    feature_dicos[(cluster,feature_id,index)] = val
    feature_ids.add(feature_id)
    cluster_ids.add(cluster)
    index_ids.add(index)

  header='header,meta:3,double:%s'%len(index_ids)    
  fout.write('%s\n'%header)
    
  for cluster in cluster_ids:
    for feature_id in range(0,nb_features):
      ltp = '%s,0,%s'%(cluster,feature_id)
      key_to_print = ''
      for i in index_ids:
        if (cluster,feature_id,i) in feature_dicos:
          key_value = '%s:%s'%(i,feature_dicos[(cluster,feature_id,i)])
        else:
          key_value = '%s:0.0'%i   
        key_to_print = ",".join([key_to_print,key_value])
        ltp=",".join([ltp,key_value])      
      
      fout.write('%s\n'%ltp)


def revert_centroid(filein, fileout):
  try: 
    fin = open(filein, 'r')
    fin.readline()
  except IOError:
    print "--- INFO: centroid.dlm file is not available"
    return

  fout = open(fileout, 'w')

  dicos={}
  cluster_ids=set()
  index_ids=set()

  for line in fin:
    record = line.replace("\n","").replace('"','').split("|")
    cluster = int(record[0])
    index = int(record[1])
    val = float(record[2])
    dicos[(cluster,index)] = val
    cluster_ids.add(cluster)
    index_ids.add(index)

  header = 'header,meta:3,double:%s'%len(index_ids)    
  fout.write('%s\n'%header)

  for cluster in cluster_ids:
    ltp = "%s,0,0"%(cluster+1)
    
    key_to_print = ''
    
    for index in range(0,len(index_ids)):
      key_value = '%s:%s'%(index,dicos[(cluster,index)])
      key_to_print = ",".join([key_to_print,key_value])
      ltp = ",".join([ltp,key_value])
    
    fout.write('%s,\n'%ltp)



#--- Read in current dictionay & update coeffs from previous run
(dictionary,feature_count)=read_dictionary(dictionary_file)
revert_coeffs0(input_folder  + "/coeffs0.dlm",  output_folder + "/coeffs0")
revert_coeffs1(input_folder  + "/coeffs1.dlm",  output_folder + "/coeffs1", dictionary,feature_count)
revert_coeffs2(input_folder  + "/coeffs2.dlm",  output_folder + "/coeffs2", dictionary,feature_count)
revert_centroid(input_folder + "/coeffscentroid.dlm", output_folder + "/centroid")
