#! /usr/bin/env bash

set -e

workspace=${1}

#--- Import sample data & forecast using default settings
partition="sample"
bash script/import.sh ${workspace}
bash script/orchestrate-ml.sh ${partition} ${workspace} config/orchestrate-ml-settings.dlm 
