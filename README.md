# README #

This repository contains the source code of template forecasting application.


# Install upstream dependencies

Before you can build the app, you need to download the application dependencies. To do so, run the following command from the project folder:

    ./script/get-dev-deps

from the command line of a terminal - the working directory is the one corresponding to the project. (NB All the examples of commands will implicitly assume the working directory to be that of the project, unless stated otherwise).

This step creates a directory called 'upstream' that contains the
upstream dependencies (i.e. latest logicblox and foula builds).


# Set up environment

To set up your development environment, issue the following command from the
project folder:

    source ./script/env.sh

The next step is to start lb services:

    lb services start


# Build the project & install 

To build the project, you must first instantiate a build configuration, namely a Makefile. For this execute:

    lb config

Then compile the code and "install" it (i.e all relevant parts will be copied into out/ folder, a.k.a deployment folder)

    make && make install

To perform a clean build run first the following command

    make clean

To perform a full build from scratch execute

    make spotless && lb config && make && make install


# Sample end-to-end run

After building the project and installing it, and setting up the data/ folder (check scripts/import.sh to see to proprely 
set it the data/ folder) you can execute an end-to-end run :

    bash script/end-to-end-sample.sh demand-sample

It will trigger the import of the data into "demand-sample" workspace and then call ML related scripts :

   bash script/orchestrate-ml.sh sample demand-sample config/orchestrate-ml-settings.dlm

where "sample" is the data partition name (can be whatever), "demand-sample" the workspace, and 
"config/orchestrate-ml-sample-settings.dlm" points to the configuration file to use.
Bellow are some details regarding the settings for different parameters in the configuration file:

* FORECAST_SETTINGS_FILE : file containing the forecasting horizon settings. E.g. data/common/forecast-domain-settings.dat
* EXCLUDED_ATTRIBUTES_FILE : file under config/ that lists all attributes to be excluded during machine learning run for this partition. E.g excluded_attributes_default
* EXCLUDED_FEATURE_FILE : file under config/ that lists all features to be excluded during machine learning run for this partition. E.g.  excluded_features_default
* MODEL : machine learning method to use. E.g. FaMa (Note that a corresponding script/FaMa.sh must exist that implements the model)
* SAMPLING_FRACTION : sampling fraction to use. Usually we sample less that 100% for very large data sets. E.g. 1.0 (i.e. 100% sampling)
* START_LEARNING_TYPE : How to initialize the machine learning run: "cold" or "warm". "cold" indicates starting from a random initial solution, while "warm" indicates starting from a solution found in a previous run (AKA "incremental learning"). E.g. cold

The results of the above sample run (forecasts, reports, run details etc) can be found under export/sample/ folder.

